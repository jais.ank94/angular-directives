import { Directive, ElementRef, Renderer2, OnInit, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit {

  //value in brakets are actual css values
  @HostBinding('style.backgroundColor') background:string;

  constructor(private elRef: ElementRef, private renderer: Renderer2) { }

  ngOnInit(){
    //1
    //this.renderer.setStyle(this.elRef.nativeElement,'background-color','blue');
  }

  @HostListener('mouseover') mouseOver(eventData:Event){
    //2
    //this.renderer.setStyle(this.elRef.nativeElement,'background-color','blue');
    //3
    this.background='blue';
  }

  @HostListener('mouseleave') mouseLeave(eventData:Event){
    //2
    //this.renderer.setStyle(this.elRef.nativeElement,'background-color','transparent');
    //3
    this.background='transparent';
  }

}
